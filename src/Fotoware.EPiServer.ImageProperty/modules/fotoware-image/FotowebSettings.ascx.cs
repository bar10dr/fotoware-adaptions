﻿using System.Web.UI;
using EPiServer.Personalization;
using EPiServer.PlugIn;

namespace Fotoware.EPiServer.ImageProperty.modules
{
    [GuiPlugIn(DisplayName = "Fotoweb Settings", Area = PlugInArea.SidSettingsArea,
        Url = "~/modules/fotoware-image/FotowebSettings.ascx")]
    public partial class FotowebSettings : UserControl, IUserSettings, ICustomPlugInLoader
    {
        public void LoadSettings(string userName, EPiServerProfile data)
        {
            if (!IsPostBack)
            {
                var existingValue = data[Constants.ProfileFieldName] as string;
                Username.Text = existingValue;
            }
        }

        public void SaveSettings(string userName, EPiServerProfile data)
        {
            data[Constants.ProfileFieldName] = Username.Text;
            data.Save();
        }

        public bool SaveRequiresUIReload { get; set; }
        public PlugInDescriptor[] List()
        {
            return new[] { PlugInDescriptor.Load(typeof(FotowebSettings)) };
        }
    }
}